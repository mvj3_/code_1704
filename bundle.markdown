在网络情况不好的情况下，运行bundle install可能会出现如下报错信息：

```text
Fetching gem metadata from http://rubygems.org/.Unfortunately, a fatal error has occurred. Please see the Bundler 
troubleshooting documentation at http://bit.ly/bundler-issues. Thanks! 
/home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:762:in `initialize': Connection timed out - connect(2) (Errno::ETIMEDOUT)
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:762:in `open'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:762:in `block in connect'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/timeout.rb:54:in `timeout'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/timeout.rb:99:in `timeout'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:762:in `connect'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:755:in `do_start'
	from /home/you/.rvm/rubies/ruby-1.9.3-p194/lib/ruby/1.9.1/net/http.rb:750:in `start'
	from /home/you/.rvm/gems/ruby-1.9.3-p194@global/gems/bundler-1.2.1/lib/bundler/vendor/net/http/persistent.rb:224:in `connection_for'
	from /home/you/.rvm/gems/ruby-1.9.3-p194@global/gems/bundler-1.2.1/lib/bundler/vendor/net/http/persistent.rb:358:in `request'
	from /home/you/.rvm/gems/ruby-1.9.3-p194@global/gems/bundler-1.2.1/lib/bundler/fetcher.rb:149:in `fetch'
```

## 解决方法是用本地安装，当然前提还是你得有获取gem的途径。

###  如果没有压缩包，那么可以从源码安装，推荐github.com下载。然后自行gem build && gem install。

##    以下是如何直接把本地的gem压缩包拷到目标电脑上的步骤：

1.gem压缩包路径可用gem env或rvm info获得。

2.然后用scp传输，例如：

```zsh
scp /usr/local/rvm/gems/ruby-1.9.3-p392/cache/acts_as_time_racing-0.2.gem you@yourhost:/home/you/.rvm/gems/ruby-1.9.3-p194/cache/
```

3.安装gem到目标机器

```zsh
gem install /home/you/.rvm/gems/ruby-1.9.3-p194/cache/acts_as_time_racing-0.2.gem
```

4.在bundle的项目目录里运行

```zsh
bundle install --local
```

5.大功告成！